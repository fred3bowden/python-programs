age = 23
cash_on_hand = 5

if age > 17 and cash_on_hand > 8:
    print("You can buy a lottery ticket.")
    print("How many would you like?")
elif age > 17 and cash_on_hand <= 8:
    print("You don't have enough money.")
    print("Please, come back when you get more.")
else:
    print("You may not buy a lottery ticket.")
    print("Can I interest you in some candy?")

print("Thank you for your patronage.")
