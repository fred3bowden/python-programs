age_string = int(input("How old are you? "))
print("Next year, you'll be", age_string + 1, "years old")

name = input("Please type your first name: ")
name_length = len(name)

if name_length > 8:
    print("You have a long name!")
else:
    print("Your name is nice and short.")
